<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Proposal;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use kartik\mpdf\Pdf;

/**
 * LogController implements the CRUD actions for Log model.
 */
class ReportController extends Controller
{
    public function generatePdf($title, $view, $params = [], $landscape = false)
    {

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => 'A4',
            'orientation' => $landscape ? 'L' : 'P',
            // 'marginTop' => '35',
            'marginTop' => '5',
            'filename' => $title,
            'options' => ['title' => $title],
            'content' => $this->renderPartial($view, $params),
            'methods' => [
                // 'SetHeader' => \backend\helpers\ReportHelper::header($title, 1),
                'SetFooter' => ['|Page {PAGENO} of {nbpg}|'],
            ],
            'cssInline' => '
                .table-report th {text-align:center} 
                .table-report th, .table-report td {border:1px solid #aaa; padding:2px 4px;} 
                .table-report td {vertical-align:top}
            ',
        ]);
        return $pdf->render();
    }

    public function actionPengajuan($tahun = '', $sumber_dana_id = '', $jenis_kegiatan_id = '', $to_pdf = false) 
    {
        $query = Proposal::find()->where(['tanggal_disposisi' => null]);
        if ($tahun) $query->andWhere(['year(tanggal_pengajuan)' => $tahun]);
        if ($sumber_dana_id) $query->andWhere(['sumber_dana_id' => $sumber_dana_id]);
        if ($jenis_kegiatan_id) $query->andWhere(['jenis_kegiatan_id' => $jenis_kegiatan_id]);
        
        $models = $query->all();
        $title = 'LAPORAN PENGAJUAN PROPOSAL';
        $view = 'pengajuan';
        $params = [
            'models' => $models,
            'title' => $title,
            'to_pdf' => $to_pdf,
            'tahun' => $tahun,
            'sumber_dana_id' => $sumber_dana_id,
            'jenis_kegiatan_id' => $jenis_kegiatan_id,
        ];
        if ($to_pdf) return $this->generatePdf($title, $view, $params, 1);
        return $this->render($view, $params);
    }

    public function actionReview($tahun = '', $sumber_dana_id = '', $jenis_kegiatan_id = '', $to_pdf = false)
    {
        $query = Proposal::find()
            ->where(['is not', 'tanggal_disposisi', null])
            ->andWhere(['!=', 'hasil_review', 1]);
        if ($tahun) $query->andWhere(['year(tanggal_pengajuan)' => $tahun]);
        if ($sumber_dana_id) $query->andWhere(['sumber_dana_id' => $sumber_dana_id]);
        if ($jenis_kegiatan_id) $query->andWhere(['jenis_kegiatan_id' => $jenis_kegiatan_id]);

        $models = $query->all();
        $title = 'LAPORAN PROPOSAL PROSES REVIEW';
        $view = 'review';
        $params = [
            'models' => $models,
            'title' => $title,
            'to_pdf' => $to_pdf,
            'tahun' => $tahun,
            'sumber_dana_id' => $sumber_dana_id,
            'jenis_kegiatan_id' => $jenis_kegiatan_id,
        ];
        if ($to_pdf) return $this->generatePdf($title, $view, $params, 1);
        return $this->render($view, $params);
    }

    public function actionAccepted($tahun = '', $sumber_dana_id = '', $jenis_kegiatan_id = '', $to_pdf = false)
    {
        $query = Proposal::find()
            ->where(['hasil_review' => 1])
            ->andWhere(['tanggal_laporan_akhir' => null]);
        if ($tahun) $query->andWhere(['year(tanggal_pengajuan)' => $tahun]);
        if ($sumber_dana_id) $query->andWhere(['sumber_dana_id' => $sumber_dana_id]);
        if ($jenis_kegiatan_id) $query->andWhere(['jenis_kegiatan_id' => $jenis_kegiatan_id]);

        $models = $query->all();
        $title = 'LAPORAN PROPOSAL LULUS REVIEW';
        $view = 'accepted';
        $params = [
            'models' => $models,
            'title' => $title,
            'to_pdf' => $to_pdf,
            'tahun' => $tahun,
            'sumber_dana_id' => $sumber_dana_id,
            'jenis_kegiatan_id' => $jenis_kegiatan_id,
        ];
        if ($to_pdf) return $this->generatePdf($title, $view, $params, 1);
        return $this->render($view, $params);
    }

    public function actionClosed($tahun = '', $sumber_dana_id = '', $jenis_kegiatan_id = '', $luaran_id = '', $to_pdf = false)
    {
        $query = Proposal::find()->where(['is not', 'tanggal_laporan_akhir', null]);
        if ($tahun) $query->andWhere(['year(tanggal_laporan_akhir)' => $tahun]);
        if ($sumber_dana_id) $query->andWhere(['sumber_dana_id' => $sumber_dana_id]);
        if ($jenis_kegiatan_id) $query->andWhere(['jenis_kegiatan_id' => $jenis_kegiatan_id]);
        if ($luaran_id) $query->joinWith(['proposalLuarans'])->andWhere(['luaran_id' => $luaran_id]);

        // echo $query->createCommand()->rawSql; die();
        $models = $query->all();
        $title = 'REKAPITULASI PENELITIAN DAN PENGABDIAN KEPADA MASYARAKAT';
        $view = 'closed';
        $params = [
            'models' => $models,
            'title' => $title,
            'to_pdf' => $to_pdf,
            'tahun' => $tahun,
            'sumber_dana_id' => $sumber_dana_id,
            'jenis_kegiatan_id' => $jenis_kegiatan_id,
            'luaran_id' => $luaran_id,
        ];
        if ($to_pdf) return $this->generatePdf($title, $view, $params, 1);
        return $this->render($view, $params);
    }

    public function actionCount($tahun = '', $sumber_dana_id = '', $jenis_kegiatan_id = '', $luaran_id = '', $to_pdf = false)
    {
        $query = Proposal::find()
            ->select('proposal.id as id, year(tanggal_pengajuan) as tahun, count(proposal.id) as jumlah_proposal, sum(total_biaya) as total_biaya')
            ->where(['is not', 'proposal.id', null]);
        if ($tahun) $query->andWhere(['year(tanggal_laporan_akhir)' => $tahun]);
        if ($sumber_dana_id) $query->andWhere(['sumber_dana_id' => $sumber_dana_id]);
        if ($jenis_kegiatan_id) $query->andWhere(['jenis_kegiatan_id' => $jenis_kegiatan_id]);
        if ($luaran_id) $query->joinWith(['proposalLuarans'])->andWhere(['luaran_id' => $luaran_id]);
        $query->groupBy('tahun');

        // echo $query->createCommand()->rawSql; die();
        $models = $query->asArray()->all();
        // dd($models);
        $title = 'REKAPITULASI JUMLAH PROPOSAL PER TAHUN';
        $view = 'count';
        $params = [
            'models' => $models,
            'title' => $title,
            'to_pdf' => $to_pdf,
            'tahun' => $tahun,
            'sumber_dana_id' => $sumber_dana_id,
            'jenis_kegiatan_id' => $jenis_kegiatan_id,
            'luaran_id' => $luaran_id,
        ];
        if ($to_pdf) return $this->generatePdf($title, $view, $params, 1);
        return $this->render($view, $params);
    }

    public function actionLuaran($tahun = '', $sumber_dana_id = '', $jenis_kegiatan_id = '', $luaran_id = '', $to_pdf = false)
    {
        $query = Proposal::find()
            ->joinWith(['luarans'])
            ->select('luaran.id as id, luaran.nama as luaran_nama, year(tanggal_laporan_akhir) as tahun, count(proposal.id) as jumlah_proposal, sum(total_biaya) as total_biaya')
            ->where(['is not', 'proposal.id', null]);
        if ($tahun) $query->andWhere(['year(tanggal_laporan_akhir)' => $tahun]);
        if ($sumber_dana_id) $query->andWhere(['sumber_dana_id' => $sumber_dana_id]);
        if ($jenis_kegiatan_id) $query->andWhere(['jenis_kegiatan_id' => $jenis_kegiatan_id]);
        if ($luaran_id) $query->andWhere(['luaran_id' => $luaran_id]);
        $query->groupBy('luaran.id');

        // echo $query->createCommand()->rawSql; die();
        $models = $query->asArray()->all();
        // dd($models);
        $title = 'REKAPITULASI LUARAN';
        if ($jenis_kegiatan_id) $title.= ' ' . strtoupper(JenisKegiatan::findOne($jenis_kegiatan_id)->nama);
        if ($sumber_dana_id) $title.= ' SUMBER DANA ' . strtoupper(SumberDana::findOne($sumber_dana_id)->nama);
        if ($tahun) $title.= ' TAHUN ' . $tahun;

        $view = 'luaran';
        $params = [
            'models' => $models,
            'title' => $title,
            'to_pdf' => $to_pdf,
            'tahun' => $tahun,
            'sumber_dana_id' => $sumber_dana_id,
            'jenis_kegiatan_id' => $jenis_kegiatan_id,
            'luaran_id' => $luaran_id,
        ];
        if ($to_pdf) return $this->generatePdf($title, $view, $params, 1);
        return $this->render($view, $params);
    }
}
