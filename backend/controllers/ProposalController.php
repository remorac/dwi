<?php

namespace backend\controllers;

use Yii;
use backend\models\Proposal;
use backend\models\ProposalSearch;
use backend\models\ProposalFile;
use backend\models\ProposalFileSearch;
use backend\models\ProposalLuaran;
use backend\models\ProposalLuaranSearch;
use backend\models\Anggota;
use backend\models\AnggotaSearch;
use backend\models\LaporanKemajuan;
use backend\models\LaporanKemajuanSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

/**
 * ProposalController implements the CRUD actions for Proposal model.
 */
class ProposalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proposal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProposalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proposal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $queryParams = Yii::$app->request->queryParams;

        $searchModel = new LaporanKemajuanSearch();
        if (!isset($queryParams['LaporanKemajuanSearch'])) $queryParams['LaporanKemajuanSearch'] = [];
        $queryParams['LaporanKemajuanSearch'] = array_merge($queryParams['LaporanKemajuanSearch'], ['proposal_id' => $id]);
        $dataProvider = $searchModel->search($queryParams);
        $dataProvider->pagination = false;

        $searchModelFile = new ProposalFileSearch();
        if (!isset($queryParams['ProposalFileSearch'])) $queryParams['ProposalFileSearch'] = [];
        $queryParams['ProposalFileSearch'] = array_merge($queryParams['ProposalFileSearch'], ['proposal_id' => $id]);
        $dataProviderFile = $searchModelFile->search($queryParams);
        $dataProviderFile->pagination = false;

        $searchModelLuaran = new ProposalLuaranSearch();
        if (!isset($queryParams['ProposalLuaranSearch'])) $queryParams['ProposalLuaranSearch'] = [];
        $queryParams['ProposalLuaranSearch'] = array_merge($queryParams['ProposalLuaranSearch'], ['proposal_id' => $id]);
        $dataProviderLuaran = $searchModelLuaran->search($queryParams);
        $dataProviderLuaran->pagination = false;

        $searchModelAnggota = new AnggotaSearch();
        if (!isset($queryParams['AnggotaSearch'])) $queryParams['AnggotaSearch'] = [];
        $queryParams['AnggotaSearch'] = array_merge($queryParams['AnggotaSearch'], ['proposal_id' => $id]);
        $dataProviderAnggota = $searchModelAnggota->search($queryParams);
        $dataProviderAnggota->pagination = false;

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelFile' => $searchModelFile,
            'dataProviderFile' => $dataProviderFile,
            'searchModelLuaran' => $searchModelLuaran,
            'dataProviderLuaran' => $dataProviderLuaran,
            'searchModelAnggota' => $searchModelAnggota,
            'dataProviderAnggota' => $dataProviderAnggota,
        ]);
    }

    /**
     * Creates a new Proposal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proposal();
        $model->dosen_user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // print '<pre>'; print_r($model); die();
            foreach ($model->luarans as $luaran) {
                $modelPL = new ProposalLuaran();
                $modelPL->proposal_id = $model->id;
                $modelPL->luaran_id = $luaran;
                $modelPL->save();
            }
            if (($uploadedFile = UploadedFile::getInstance($model, 'file_proposal')) !== null) {
                $modelFile = new ProposalFile();
                $modelFile->proposal_id = $model->id;
                $modelFile->tanggal_upload = date('Y-m-d H:i:s');
                if ($modelFile->save()) {
                    $filename = 'proposalfile' . $modelFile->id . '.' . $uploadedFile->extension;
                    $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                    $modelFile->file_proposal = $filename;
                    $modelFile->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Proposal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /* if (($uploadedFile = UploadedFile::getInstance($model, 'file_proposal')) !== null) {
                $filename = 'proposal' . $model->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $model->file_proposal = $filename;
                $model->save();
            } */
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDisposisi($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('disposisi', [
                'model' => $model,
            ]);
        }
    }

    public function actionReview($id)
    {
        $model = $this->findModel($id);
        $model->tanggal_review = date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (($uploadedFile = UploadedFile::getInstance($model, 'file_review')) !== null) {
                $filename = 'review' . $model->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $model->file_review = $filename;
                $model->save();
            }
            if (($uploadedFile = UploadedFile::getInstance($model, 'file_penilaian')) !== null) {
                $filename = 'penilaian' . $model->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $model->file_penilaian = $filename;
                $model->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('review', [
                'model' => $model,
            ]);
        }
    }


    public function actionAnggota($id)
    {
        $model = $this->findModel($id);
        $modelLK = new Anggota();
        $modelLK->proposal_id = $model->id;

        if ($modelLK->load(Yii::$app->request->post()) && $modelLK->save()) {
            return $this->redirect(['anggota', 'id' => $modelLK->proposal_id]);
        }

        $searchModel = new AnggotaSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['AnggotaSearch'])) $queryParams['AnggotaSearch'] = [];
        $queryParams['AnggotaSearch'] = array_merge($queryParams['AnggotaSearch'], ['proposal_id' => $model->id]);
        $dataProvider = $searchModel->search($queryParams);
        $dataProvider->pagination = false;

        return $this->render('anggota', [
            'model' => $model,
            'modelLK' => $modelLK,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAnggotaDelete($id)
    {
        try {
            $modelLK = Anggota::findOne($id);
            $modelLK->delete();
            return $this->redirect(['anggota', 'id' => $modelLK->proposal_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500, "Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }


    public function actionProposalFile($id)
    {
        $model = $this->findModel($id);
        $modelLK = new ProposalFile();
        $modelLK->proposal_id = $model->id;
        $modelLK->tanggal_upload = date('Y-m-d H:i:s');

        if ($modelLK->load(Yii::$app->request->post()) && $modelLK->save()) {
            if (($uploadedFile = UploadedFile::getInstance($modelLK, 'file_proposal')) !== null) {
                $filename = 'proposalfile' . $modelLK->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $modelLK->file_proposal = $filename;
                $modelLK->save();
                return $this->redirect(['proposal-file', 'id' => $modelLK->proposal_id]);
            }
        }

        $searchModel = new ProposalFileSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['ProposalFileSearch'])) $queryParams['ProposalFileSearch'] = [];
        $queryParams['ProposalFileSearch'] = array_merge($queryParams['ProposalFileSearch'], ['proposal_id' => $model->id]);
        $dataProvider = $searchModel->search($queryParams);
        $dataProvider->pagination = false;

        return $this->render('proposal-file', [
            'model' => $model,
            'modelLK' => $modelLK,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProposalFileDelete($id)
    {
        try {
            $modelLK = ProposalFile::findOne($id);
            $modelLK->delete();
            unlink(Yii::getAlias('@upload/'.$modelLK->file_proposal));
            return $this->redirect(['proposal-file', 'id' => $modelLK->proposal_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500, "Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    public function actionProposalLuaran($id)
    {
        $post = Yii::$app->request->post(); 

        $model = $this->findModel($id);
        $modelLK = new ProposalLuaran();
        $modelLK->proposal_id = $model->id;

        if ($modelLK->load($post)) {
            if ($modelLK->save()) return $this->redirect(['proposal-luaran', 'id' => $id]);           
        }

        if ($post) {
            $modelLuaran = ProposalLuaran::findOne($post['link_id']);
            if (isset($post['link_luaran']) && $post['link_luaran']) {
                $modelLuaran->link_luaran = $post['link_luaran'];
            }
            if (isset($post['keterangan']) && $post['keterangan']) {
                $modelLuaran->keterangan = $post['keterangan'];
            }
            if (($uploadedFile = UploadedFile::getInstanceByName('file_luaran')) !== null) {
                $filename = 'proposalluaran' . $modelLK->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $modelLuaran->file_luaran = $filename;
            }
            if ($modelLuaran->save()) return $this->redirect(['proposal-luaran', 'id' => $id]);
        }

        $searchModel = new ProposalLuaranSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['ProposalLuaranSearch'])) $queryParams['ProposalLuaranSearch'] = [];
        $queryParams['ProposalLuaranSearch'] = array_merge($queryParams['ProposalLuaranSearch'], ['proposal_id' => $model->id]);
        $dataProvider = $searchModel->search($queryParams);
        $dataProvider->pagination = false;

        return $this->render('proposal-luaran', [
            'model' => $model,
            'modelLK' => $modelLK,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProposalLuaranDelete($id)
    {
        try {
            $modelLK = ProposalLuaran::findOne($id);
            $modelLK->delete();
            return $this->redirect(['proposal-luaran', 'id' => $modelLK->proposal_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500, "Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }



    public function actionLaporanKemajuan($id)
    {
        $model = $this->findModel($id);
        $modelLK = new LaporanKemajuan();
        $modelLK->proposal_id = $model->id;

        if ($modelLK->load(Yii::$app->request->post()) && $modelLK->save()) {
            if (($uploadedFile = UploadedFile::getInstance($modelLK, 'file_laporan')) !== null) {
                $filename = 'laporankemajuan' . $modelLK->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $modelLK->file_laporan = $filename;
                $modelLK->save();
                return $this->redirect(['laporan-kemajuan', 'id' => $modelLK->proposal_id]);
            }
            $modelLK = new LaporanKemajuan();
        }

        $searchModel = new LaporanKemajuanSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['LaporanKemajuanSearch'])) $queryParams['LaporanKemajuanSearch'] = [];
        $queryParams['LaporanKemajuanSearch'] = array_merge($queryParams['LaporanKemajuanSearch'], ['proposal_id' => $model->id]);
        $dataProvider = $searchModel->search($queryParams);
        $dataProvider->pagination = false;

        return $this->render('laporan-kemajuan', [
            'model' => $model,
            'modelLK' => $modelLK,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLaporanKemajuanDelete($id)
    {
        try {
            $modelLK = LaporanKemajuan::findOne($id);
            $modelLK->delete();
            return $this->redirect(['laporan-kemajuan', 'id' => $modelLK->proposal_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500, "Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    public function actionLaporanAkhir($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (($uploadedFile = UploadedFile::getInstance($model, 'file_laporan_akhir')) !== null) {
                $filename = 'laporan_akhir' . $model->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $model->file_laporan_akhir = $filename;
                $model->save();
            }
            if (($uploadedFile = UploadedFile::getInstance($model, 'file_luaran')) !== null) {
                $filename = 'luaran' . $model->id . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs(Yii::getAlias('@uploads/' . $filename));
                $model->file_luaran = $filename;
                $model->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('laporan-akhir', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proposal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
            ProposalLuaran::deleteAll(['proposal_id' => $model->id]);
            ProposalFile::deleteAll(['proposal_id' => $model->id]);
            $model->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Proposal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proposal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proposal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownload($filename, $download_name = '')
    {
        if (!$download_name) $download_name = $filename;
        $filepath = Yii::getAlias('@uploads/' . $filename);
        if (is_file($filepath)) Yii::$app->response->sendFile($filepath, $download_name, ['inline' => true]);
    }
}
