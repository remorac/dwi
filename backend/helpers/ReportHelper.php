<?php

namespace backend\helpers;

use yii;
use yii\helpers\Url;
use backend\models\ReportSigner;

class ReportHelper
{
	public static function header($title, $to_pdf = false) {
		$imageWidth 	= $to_pdf ? '70px' : '50%';
		$marginBottom 	= $to_pdf ? '10px' : '20px';
		return '
			<table width="100%" style="margin-bottom:10px">
				<tr>
					<td align="left" valign="center" width="15%"><img src="'.\Yii::getAlias('@web/img/logo-stmik.jpeg').'" width="'.$imageWidth.'"></td>
					<td align="center" valign="center">
						<font size="4"><br>STMIK INDONESIA PADANG </font>				
						<br><font size="5">L P P M</font>
						<br>
						<br>'.$title.'
					</td>
					<td width="15%">&nbsp;</td>
				</tr>
			</table>
		';
	}

	public static function footer($to_pdf = false) {
		$fontSize 	= $to_pdf ? 'font-size:11px' : '';
		$ketuaLppm 	= (isset(Yii::$app->params['ketuaLppm'])) ? Yii::$app->params['ketuaLppm'] : 'Restyaliza Dhini Hary, M.Pd';

		return '
			<table width="100%" style="margin-bottom:20px;'.$fontSize. '">
				<tr><td colspan=3 class="text-center"></td></tr>
				<tr>
					<td width="33%" align="center">
						<br>Padang, ' . date('d F Y') . '
						<br><b>Ketua LPPM</b>
						<br>
						<br>
						<br>
						<br>
						<br><b>' . $ketuaLppm . '</b>
						<br>
					</td>
					<td width="34%" align="center">						
						<br>
						<br><b></b>
						<br>
						<br>
						<br>
						<br>
						<br><b><u></u></b>
						<br>
					</td>
					<td width="33%" align="center">						
						<br>
						<br><b></b>
						<br>
						<br>
						<br>
						<br>
						<br><b><u></u></b>
						<br>
					</td>
				</tr>
			</table>
		';
	}
}
