<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "laporan_kemajuan".
 *
 * @property integer $id
 * @property integer $proposal_id
 * @property integer $tanggal_laporan
 * @property string $file_laporan
 *
 * @property Proposal $proposal
 * @property Proposal $tanggalLaporan
 */
class LaporanKemajuan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laporan_kemajuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proposal_id', 'tanggal_laporan'], 'required'],
            [['proposal_id'], 'integer'],
            [['tanggal_laporan'], 'safe'],
            [['file_laporan'], 'string'],
            [['proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proposal::className(), 'targetAttribute' => ['proposal_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_id' => 'Proposal',
            'tanggal_laporan' => 'Tanggal Laporan',
            'file_laporan' => 'File Laporan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposal()
    {
        return $this->hasOne(Proposal::className(), ['id' => 'proposal_id']);
    }

    public function getName()
    {
        $array = explode('.', $this->file_laporan);
        $extension = end($array);
        return 'Laporan Kemajuan ' . $this->tanggal_laporan . '.' . $extension;
    }
}
