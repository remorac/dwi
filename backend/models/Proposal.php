<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "proposal".
 *
 * @property integer $id
 * @property string $judul
 * @property integer $dosen_user_id
 * @property integer $jenis_kegiatan_id
 * @property string $tanggal_pengajuan
 * @property integer $total_biaya
 * @property integer $sumber_dana_id
 * @property string $luaran
 * @property string $tanggal_disposisi
 * @property integer $reviewer_user_id
 * @property string $tanggal_review
 * @property integer $hasil_review
 * @property string $keterangan_review
 * @property string $file_review
 * @property string $tanggal_laporan_akhir
 * @property string $file_laporan_akhir
 * @property string $file_luaran
 *
 * @property LaporanKemajuan[] $laporanKemajuans
 * @property User $dosenUser
 * @property JenisKegiatan $jenisKegiatan
 * @property SumberDana $sumberDana
 * @property User $reviewerUser
 * @property ProposalFile[] $proposalFiles
 * @property ProposalLuaran[] $proposalLuarans
 */
class Proposal extends \yii\db\ActiveRecord
{
    public $file_proposal;
    public $luarans;

    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal';
    }

    public static function hasilReviews($index = 'all') {
        $array = [
            1 => 'Diterima tanpa revisi',
            2 => 'Diterima dengan revisi',
            0 => 'Ditolak',
        ];
        if ($index == 'all') return $array;
        if (isset($array[$index])) return $array[$index];
        return false;
    }

    /* public static function luarans($index = 'all')
    {
        $array = [
            0 => 'Publikasi Ilmiah',
            1 => 'HKI',
            2 => 'Buku Ajar',
        ];
        if ($index == 'all') return $array;
        if (isset($array[$index])) return $array[$index];
        return false;
    } */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'dosen_user_id', 'jenis_kegiatan_id', 'tanggal_pengajuan', 'total_biaya', 'sumber_dana_id'], 'required'],
            [['dosen_user_id', 'jenis_kegiatan_id', 'total_biaya', 'sumber_dana_id', 'reviewer_user_id', 'hasil_review'], 'integer'],
            [['tanggal_pengajuan', 'tanggal_disposisi', 'tanggal_review', 'tanggal_laporan_akhir'], 'safe'],
            [['ringkasan', 'keterangan_review', 'file_review', 'file_penilaian', 'file_laporan_akhir'], 'string'],
            [['judul'], 'string', 'max' => 191],
            [['dosen_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['dosen_user_id' => 'id']],
            [['jenis_kegiatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisKegiatan::className(), 'targetAttribute' => ['jenis_kegiatan_id' => 'id']],
            [['sumber_dana_id'], 'exist', 'skipOnError' => true, 'targetClass' => SumberDana::className(), 'targetAttribute' => ['sumber_dana_id' => 'id']],
            [['reviewer_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['reviewer_user_id' => 'id']],
            [['luarans'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'dosen_user_id' => 'Dosen',
            'jenis_kegiatan_id' => 'Jenis Kegiatan',
            'tanggal_pengajuan' => 'Tanggal Pengajuan',
            'total_biaya' => 'Total Biaya',
            'sumber_dana_id' => 'Sumber Dana',
            'tanggal_disposisi' => 'Tanggal Disposisi',
            'reviewer_user_id' => 'Reviewer',
            'tanggal_review' => 'Tanggal Review',
            'hasil_review' => 'Hasil Review',
            'keterangan_review' => 'Keterangan Review',
            'file_review' => 'File Review',
            'tanggal_laporan_akhir' => 'Tanggal Laporan Akhir',
            'file_laporan_akhir' => 'File Laporan Akhir',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaporanKemajuans()
    {
        return $this->hasMany(LaporanKemajuan::className(), ['proposal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosenUser()
    {
        return $this->hasOne(User::className(), ['id' => 'dosen_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisKegiatan()
    {
        return $this->hasOne(JenisKegiatan::className(), ['id' => 'jenis_kegiatan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSumberDana()
    {
        return $this->hasOne(SumberDana::className(), ['id' => 'sumber_dana_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewerUser()
    {
        return $this->hasOne(User::className(), ['id' => 'reviewer_user_id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */
    public function getProposalFiles()
    {
        return $this->hasMany(ProposalFile::className(), ['proposal_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */
    public function getProposalLuarans()
    {
        return $this->hasMany(ProposalLuaran::className(), ['proposal_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */
    public function getAnggotas()
    {
        return $this->hasMany(Anggota::className(), ['proposal_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */
    public function getLuarans()
    {
        return $this->hasMany(Luaran::className(), ['id' => 'luaran_id'])->viaTable('proposal_luaran', ['proposal_id' => 'id']);
    }

    public function getNameReview()
    {
        $array = explode('.', $this->file_review);
        $extension = end($array);
        return $this->judul . '.' . $extension;
    }

    public function getNamePenilaian()
    {
        $array = explode('.', $this->file_penilaian);
        $extension = end($array);
        return $this->judul . '.' . $extension;
    }

    public function getAnggotaText()
    {
        $anggotas = [];
        foreach ($this->anggotas as $anggota) {
            $anggotas[] = $anggota->nama_anggota;
        }
        $agt = $anggotas ? implode(',<br>', $anggotas) : '';
        return $agt;
    }

    public function getLuaranText()
    {
        $i = 0;
        $luarans = [];
        foreach ($this->proposalLuarans as $luaran) {
            // $luarans[] = $luaran->luaran->nama;
            $i++;
            $br = $i > 1 ? '<br>': '';  
            $pre_element = '';
            $pre_element.= $br.$luaran->luaran->nama.'<br>';
            if ($luaran->keterangan) $pre_element.= ' <i class="small">('.$luaran->keterangan.')</i>';
            if ($luaran->link_luaran) $pre_element.= ' <i class="small">('.$luaran->link_luaran.')</i>';
            $luarans[] = $pre_element;
        }
        $agt = $luarans ? implode(',', $luarans) : '';
        return $agt;
    }

    public function getNeedReview()
    {
        foreach ($this->proposalFiles as $proposalFile) {
            if ($this->hasil_review != 1 && $proposalFile->tanggal_upload > $this->tanggal_review) return true;
        }
        return false;
    }

    public function getNeedUpload()
    {
        if (!$this->proposalFiles) return true;
        foreach ($this->proposalFiles as $proposalFile) {
            if ($this->hasil_review != 1 && $proposalFile->tanggal_upload < $this->tanggal_review) return true;
        }
        return false;
    }

    public function getNextProcess() {
        if (!$this->reviewer_user_id) return 'Disposisi';
        if ($this->needUpload) return 'Upload Proposal';
        if ($this->needReview) return 'Review';
        if (!$this->tanggal_laporan_akhir) return 'Laporan';
        return '(selesai)';
    }

    public function getKode()
    {
        return date('Y', strtotime($this->tanggal_pengajuan)).'.'.$this->jenisKegiatan->kode.$this->sumberDana->kode.'.'.$this->id;
    }
}

