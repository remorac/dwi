<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "proposal_file".
 *
 * @property integer $id
 * @property integer $proposal_id
 * @property string $file_proposal
 * @property string $tanggal_upload
 *
 * @property Proposal $proposal
 */
class ProposalFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proposal_id', 'tanggal_upload'], 'required'],
            [['proposal_id'], 'integer'],
            [['file_proposal'], 'string'],
            [['tanggal_upload'], 'safe'],
            [['proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proposal::className(), 'targetAttribute' => ['proposal_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_id' => 'Proposal',
            'file_proposal' => 'File Proposal',
            'tanggal_upload' => 'Tanggal Upload',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposal()
    {
        return $this->hasOne(Proposal::className(), ['id' => 'proposal_id']);
    }

    public function getName() {
        $array = explode('.', $this->file_proposal);
        $extension = end($array);
        return $this->proposal->judul. ' ' . $this->tanggal_upload . '.' . $extension;
    }
}
