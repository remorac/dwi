<?php
 
namespace backend\models;
 
use Yii;
 
class DynamicModel extends \yii\base\DynamicModel
{    
    public function getIsNewRecord()
    { 
        return true;
    }
}