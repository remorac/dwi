<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "proposal_luaran".
 *
 * @property integer $id
 * @property integer $proposal_id
 * @property integer $luaran_id
 * @property string $link_luaran
 * @property string $file_luaran
 * @property string $keterangan
 *
 * @property Proposal $proposal
 * @property Luaran $luaran
 */
class ProposalLuaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal_luaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proposal_id', 'luaran_id'], 'required'],
            [['proposal_id', 'luaran_id'], 'integer'],
            [['link_luaran', 'file_luaran', 'keterangan'], 'string'],
            [['proposal_id', 'luaran_id'], 'unique', 'targetAttribute' => ['proposal_id', 'luaran_id'], 'message' => 'The combination of Proposal and Luaran has already been taken.'],
            [['proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proposal::className(), 'targetAttribute' => ['proposal_id' => 'id']],
            [['luaran_id'], 'exist', 'skipOnError' => true, 'targetClass' => Luaran::className(), 'targetAttribute' => ['luaran_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_id' => 'Proposal',
            'luaran_id' => 'Luaran',
            'link_luaran' => 'Link Luaran',
            'file_luaran' => 'File Luaran',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposal()
    {
        return $this->hasOne(Proposal::className(), ['id' => 'proposal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLuaran()
    {
        return $this->hasOne(Luaran::className(), ['id' => 'luaran_id']);
    }

    public function getName()
    {
        $array = explode('.', $this->file_luaran);
        $extension = end($array);
        return 'Luaran ' . $this->luaran->nama . '.' . $extension;
    } 
}
