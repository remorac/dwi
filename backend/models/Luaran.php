<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "luaran".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property ProposalLuaran[] $proposalLuarans
 */
class Luaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'luaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 191],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposalLuarans()
    {
        return $this->hasMany(ProposalLuaran::className(), ['luaran_id' => 'id']);
    }
}
