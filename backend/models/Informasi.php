<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "informasi".
 *
 * @property integer $id
 * @property string $judul
 * @property string $isi
 * @property string $waktu
 */
class Informasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'informasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'isi', 'waktu'], 'required'],
            [['isi'], 'string'],
            [['waktu'], 'safe'],
            [['judul'], 'string', 'max' => 191],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'waktu' => 'Waktu',
        ];
    }
}
