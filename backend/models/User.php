<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class User extends \yii\db\ActiveRecord
{
    public $password;
    public $role;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['username', 'email', 'status', 'role', 'password', 'full_name'];
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'email', 'status', 'role', 'password', 'full_name'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            // \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    public static function statusText($index = 'all')
    {
        $array = [
            10 => 'Active',
            0 => 'Inactive',
        ];
        if ($index == 'all') return $array;
        if (isset($array[$index])) return $array[$index];
        return false;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'full_name', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'full_name', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['role'], 'safe'],
            [['password_reset_token'], 'unique'],
            [['password'], 'required', 'on' => self::SCENARIO_CREATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username (NIDN)'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getRoles() 
    { 
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']); 
    } 


    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHolidays()
    {
        return $this->hasMany(Holiday::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHolidays0()
    {
        return $this->hasMany(Holiday::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getInspectionReports()
    {
        return $this->hasMany(InspectionReport::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getInspectionReports0()
    {
        return $this->hasMany(InspectionReport::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getInspectionReportActivities()
    {
        return $this->hasMany(InspectionReportActivity::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getInspectionReportActivities0()
    {
        return $this->hasMany(InspectionReportActivity::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPlanners()
    {
        return $this->hasMany(Planner::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPlanners0()
    {
        return $this->hasMany(Planner::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPreparers()
    {
        return $this->hasMany(Preparer::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPreparers0()
    {
        return $this->hasMany(Preparer::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPreparers1()
    {
        return $this->hasMany(Preparer::className(), ['user_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPurchaseOrderTypes()
    {
        return $this->hasMany(PurchaseOrderType::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPurchaseOrderTypes0()
    {
        return $this->hasMany(PurchaseOrderType::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTechnicalEvaluations()
    {
        return $this->hasMany(TechnicalEvaluation::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTechnicalEvaluations0()
    {
        return $this->hasMany(TechnicalEvaluation::className(), ['updated_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTechnicalEvaluationActivities()
    {
        return $this->hasMany(TechnicalEvaluationActivity::className(), ['created_by' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTechnicalEvaluationActivities0()
    {
        return $this->hasMany(TechnicalEvaluationActivity::className(), ['updated_by' => 'id']);
    }
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCurrentRole()
    {
        return $this->roles ? $this->roles[0]->item_name : null;
    }

    public function getStatusText()
    {
        return $this->status == 10 ? 'Active' : 'Inactive';
    }

    public function getLongText()
    {
        return $this->full_name.' - '.$this->username;
    }
}
