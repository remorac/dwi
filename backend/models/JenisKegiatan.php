<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jenis_kegiatan".
 *
 * @property integer $id
 * @property string $nama
 * @property string $kode
 *
 * @property Proposal[] $proposals
 */
class JenisKegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'kode'], 'required'],
            [['nama', 'kode'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'kode' => 'Kode',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposals()
    {
        return $this->hasMany(Proposal::className(), ['jenis_kegiatan_id' => 'id']);
    }
}
