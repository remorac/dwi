<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Proposal;

/**
 * ProposalSearch represents the model behind the search form about `backend\models\Proposal`.
 */
class ProposalSearch extends Proposal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'dosen_user_id', 'jenis_kegiatan_id', 'total_biaya', 'sumber_dana_id', 'reviewer_user_id', 'hasil_review'], 'integer'],
            [['judul', 'ringkasan', 'tanggal_pengajuan', 'tanggal_disposisi', 'tanggal_review', 'keterangan_review', 'file_review', 'tanggal_laporan_akhir', 'file_laporan_akhir'], 'safe'],
            [['anggotaText'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proposal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /* $dataProvider->sort->attributes['anggotaText'] = [
            'asc' => ['group_concat(anggota.nama_anggota)' => SORT_ASC],
            'desc' => ['group_concat(anggota.nama_anggota)' => SORT_DESC],
        ]; */

        $query->joinWith(['anggotas']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dosen_user_id' => $this->dosen_user_id,
            'jenis_kegiatan_id' => $this->jenis_kegiatan_id,
            'tanggal_pengajuan' => $this->tanggal_pengajuan,
            'total_biaya' => $this->total_biaya,
            'sumber_dana_id' => $this->sumber_dana_id,
            'tanggal_disposisi' => $this->tanggal_disposisi,
            'reviewer_user_id' => $this->reviewer_user_id,
            'tanggal_review' => $this->tanggal_review,
            'hasil_review' => $this->hasil_review,
            'tanggal_laporan_akhir' => $this->tanggal_laporan_akhir,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'ringkasan', $this->ringkasan])
            ->andFilterWhere(['like', 'keterangan_review', $this->keterangan_review])
            ->andFilterWhere(['like', 'file_review', $this->file_review])
            ->andFilterWhere(['like', 'file_laporan_akhir', $this->file_laporan_akhir])
            ->andFilterWhere([
                'like',
                '(select (group_concat(nama_anggota)) as anggotaText from anggota 
                    where proposal.id = anggota.proposal_id
                )',
                $this->getAttribute('anggotaText')
            ]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(
            parent::attributes(),
            [
                'anggotaText',
            ]
        );
    }
}
