<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ProposalLuaran;

/**
 * ProposalLuaranSearch represents the model behind the search form about `backend\models\ProposalLuaran`.
 */
class ProposalLuaranSearch extends ProposalLuaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'proposal_id', 'luaran_id'], 'integer'],
            [['link_luaran', 'file_luaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProposalLuaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'proposal_id' => $this->proposal_id,
            'luaran_id' => $this->luaran_id,
        ]);

        $query->andFilterWhere(['like', 'link_luaran', $this->link_luaran])
            ->andFilterWhere(['like', 'file_luaran', $this->file_luaran]);

        return $dataProvider;
    }
}
