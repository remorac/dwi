<?php

namespace backend\rbac;

use Yii;
use yii\rbac\Rule;
use backend\models\Proposal;
use backend\models\Pjs;

/**
 * Checks if authorID matches user passed via params
 */
class DosenRule extends Rule
{
    public $name = 'isDosen';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $get    = Yii::$app->request->get();
        $model  = Proposal::findOne($get['id']);

        if (isset($get['id']) && $model && $model->dosen_user_id) {
            if ($model->dosen_user_id == Yii::$app->user->id) return true;
        }
        return false;
    }
}