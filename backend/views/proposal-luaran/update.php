<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ProposalLuaran */

$this->title = 'Update Proposal Luaran: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proposal Luarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proposal-luaran-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
