<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ProposalLuaran */

$this->title = 'Create Proposal Luaran';
$this->params['breadcrumbs'][] = ['label' => 'Proposal Luarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-luaran-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
