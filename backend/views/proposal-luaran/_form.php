<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use backend\models\Proposal;
use backend\models\Luaran;

/* @var $this yii\web\View */
/* @var $model backend\models\ProposalLuaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-luaran-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'proposal_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Proposal::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'luaran_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Luaran::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'link_luaran')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_luaran')->textarea(['rows' => 6]) ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-6 col-sm-offset-3">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
