<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProposalLuaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-luaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'proposal_id') ?>

    <?= $form->field($model, 'luaran_id') ?>

    <?= $form->field($model, 'link_luaran') ?>

    <?= $form->field($model, 'file_luaran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
