<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;

$visibility = ['none', 'inline-block'];
?>

<div class="proposal-view box box-info">

    <div class="box-body">
        <h3 style="margin-top:0">PROPOSAL
        <span class="pull-right">
        <?php $index = $model->hasil_review ? 0 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        <?php $index = $model->reviewer_user_id ? 0 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'style' => 'display:' . $visibility[$index],
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </span>
        </h3>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'kode',
                'judul',
                'ringkasan:ntext',
                'dosenUser.longText:text:Dosen',
                'jenisKegiatan.nama:text:Jenis Kegiatan',
                'tanggal_pengajuan:date',
                'total_biaya:integer',
                'sumberDana.nama:text:Sumber Dana',
            ],
        ]) ?>

        <br><h3>ANGGOTA</h3>
        <p>
        <?php $index = $model->hasil_review == 1 ? 1 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . 'Manage Anggota', ['anggota', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        </p>
        <?php 
        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right serial-column'],
                'contentOptions' => ['class' => 'text-right serial-column'],
            ],
            // id,
            'nama_anggota',
        ];
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProviderAnggota,
            // 'pjax' => true,
            'hover' => true,
            'striped' => false,
            'bordered' => false,
            'toolbar' => [
                Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
                Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
                '{toggleData}',
            ],
            'panel' => false,
            'pjaxSettings' => ['options' => ['id' => 'grid']],
            // 'filterModel' => $searchModelAnggota,
            'columns' => $gridColumns,
        ]); ?>


        <br><h3>LUARAN</h3>
        <p>
        <?php $index = $model->hasil_review == 1 ? 1 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . 'Manage Luaran', ['proposal-luaran', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        </p>
        <?php 
        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right serial-column'],
                'contentOptions' => ['class' => 'text-right serial-column'],
            ],
            // id,
            'luaran.nama:text:Jenis Luaran',
            'link_luaran',
            [
                'attribute' => 'file_luaran',
                'format' => 'raw',
                'value' => function($data) { return Html::a($data->file_luaran, ['download', 'filename' => $data->file_luaran]); },
            ],
        ];
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProviderLuaran,
            // 'pjax' => true,
            'hover' => true,
            'striped' => false,
            'bordered' => false,
            'toolbar' => [
                Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
                Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
                '{toggleData}',
            ],
            'panel' => false,
            'pjaxSettings' => ['options' => ['id' => 'grid']],
            // 'filterModel' => $searchModelLuaran,
            'columns' => $gridColumns,
        ]); ?>



        <br><h3>FILE PROPOSAL</h3>
        <p>
        <?php $index = $model->hasil_review == 1 ? 0 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . 'Manage Proposal', ['proposal-file', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        </p>
        <?php 
        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right serial-column'],
                'contentOptions' => ['class' => 'text-right serial-column'],
            ],
            // id,
            /* [
                'attribute' => 'tanggal_upload',
                'format' => 'date',
            ], */
            [
                'attribute' => 'file_proposal',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->name, ['download', 'filename' => $data->file_proposal, 'download_name' => $data->name]);
                },
            ],
        ];
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProviderFile,
            // 'pjax' => true,
            'hover' => true,
            'striped' => false,
            'bordered' => false,
            'toolbar' => [
                Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
                Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
                '{toggleData}',
            ],
            'panel' => false,
            'pjaxSettings' => ['options' => ['id' => 'grid']],
            // 'filterModel' => $searchModelFile,
            'columns' => $gridColumns,
        ]); ?>


        
        <br><h3>REVIEW</h3>
        <p>
        <?php $index = !$model->reviewer_user_id ? 0 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . 'Update Review', ['review', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        </p>
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                'tanggal_disposisi:date',
                'reviewerUser.longText:text:Reviewer',
                'tanggal_review:date',
                [
                    'attribute' => 'hasil_review',
                    'value' => $model->hasil_review ? $model->hasilReviews($model->hasil_review) : '',
                ],
                'keterangan_review:ntext',
                [
                    'attribute' => 'file_review',
                    'format' => 'raw',
                    'value' => $model->file_review ? Html::a('Review - '.$model->nameReview, ['download', 'filename' => $model->file_review, 'download_name' => $model->nameReview]) : '',
                ],
                [
                    'attribute' => 'file_penilaian',
                    'format' => 'raw',
                    'value' => $model->file_penilaian ? Html::a('Penilaian - '.$model->namePenilaian, ['download', 'filename' => $model->file_penilaian, 'download_name' => $model->namePenilaian]) : '',
                ],
            ],
        ]) ?>

        <br><h3>LAPORAN KEMAJUAN</h3>
        <p>
        <?php $index = !$model->hasil_review ? 0 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . 'Manage Laporan Kemajuan', ['laporan-kemajuan', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        </p>
        <?php 
            $gridColumns = [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['class' => 'text-right serial-column'],
                    'contentOptions' => ['class' => 'text-right serial-column'],
                ],
                // 'id',
                [
                    'attribute' => 'tanggal_laporan',
                    'format' => 'date',
                ],
                [
                    'attribute' => 'file_laporan',
                    'format' => 'raw',
                    'value' => function($data) { 
                        return Html::a($data->name, ['download', 'filename' => $data->file_laporan, 'download_name' => $data->name]);
                    },
                ],
            ];
            ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'pjax' => true,
            'hover' => true,
            'striped' => false,
            'bordered' => false,
            'toolbar' => [
                Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
                Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
                '{toggleData}',
            ],
            'panel' => false,
            'pjaxSettings' => ['options' => ['id' => 'grid']],
            // 'filterModel' => $searchModel,
            'columns' => $gridColumns,
        ]); ?>
        
        <br><h3>LAPORAN AKHIR</h3>
        <p>
        <?php $index = !$model->hasil_review ? 0 : 1; ?>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . 'Update Laporan Akhir', ['laporan-akhir', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'style' => 'display:' . $visibility[$index],
        ]) ?>
        </p>
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                'tanggal_laporan_akhir:date',
				[
                    'attribute' => 'file_laporan_akhir',
                    'format' => 'raw',
                    'value' => Html::a($model->file_laporan_akhir, ['download', 'filename' => $model->file_laporan_akhir]),
                ],
            ],
        ]) ?>
    </div>
</div>
