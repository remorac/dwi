<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\datecontrol\DateControl;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Disposisi';
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = $this->title;

if (!$model->tanggal_disposisi) $model->tanggal_disposisi = date('Y-m-d');
?>

<div class="proposal-create box box-success">
	<!-- <div class="box-header"></div> -->

    <div class="box-body">

		<?= DetailView::widget([
			'options' => ['class' => 'table detail-view'],
			'model' => $model,
			'attributes' => [
							// 'id',
                'kode',
				'judul',
				'ringkasan:ntext',
				'dosenUser.longText:text:Dosen',
				'jenisKegiatan.nama:text:Jenis Kegiatan',
				'tanggal_pengajuan',
				'total_biaya:integer',
				'sumberDana.nama:text:Sumber Dana',
			],
		]) ?>
		<br>
		<br>

		<div class="proposal-form">

			<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

			<?= $form->field($model, 'tanggal_disposisi')->widget(DateControl::classname(), [
						// 'type' => DatePicker::TYPE_COMPONENT_PREPEND,
						'readonly' => true,
						'pluginOptions' => ['autoclose' => true, /* 'format' => 'yyyy-mm-dd' */],
					]); ?>

			<?= $form->field($model, 'reviewer_user_id')->widget(Select2::classname(), [
						'data' => ArrayHelper::map(User::find()->joinWith(['roles'])->where(['item_name' => 'reviewer'])->all(), 'id', 'longText'),
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					]); ?>

			
			<div class="form-panel">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Disposisi'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>

		</div>

    </div>

</div>
