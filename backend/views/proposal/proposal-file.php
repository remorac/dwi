<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'File Proposal';
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul, 'url' => ['view', 'id' => $model->id]];
?>
<div class="proposal-create box box-success">
	<!-- <div class="box-header"></div> -->

    <div class="box-body">

		<?= DetailView::widget([
			'options' => ['class' => 'table detail-view'],
			'model' => $model,
			'attributes' => [
							// 'id',
                'kode',
				'judul',
				'ringkasan:ntext',
				'dosenUser.username:text:Dosen',
				'jenisKegiatan.nama:text:Jenis Kegiatan',
				'tanggal_pengajuan:date',
				'total_biaya:integer',
				'sumberDana.nama:text:Sumber Dana',
			],
		]) ?>
		<br>
		<br>

		<div class="proposal-form">

			<?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

			<?= $form->field($modelLK, 'file_proposal')->widget(FileInput::classname(), [
				'options' => [
					'accept' => 'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				],
				'pluginOptions' => [
					'showPreview' => false,
					'showCaption' => true,
					'showRemove' => true,
					'showUpload' => false,
					'allowedFileExtensions' => ['pdf', 'PDF', 'docx', 'DOCX', 'doc', 'DOC'],
					// 'maxFileSize' => 8192,
				]
			]); ?>
			
			<div class="form-panel">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($modelLK->isNewRecord ? 'Create' : 'Update'), ['class' => $modelLK->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
		
		<div style="padding-top:20px">

		<?php 
	$gridColumns = [
		[
			'class' => 'yii\grid\SerialColumn',
			'headerOptions' => ['class' => 'text-right serial-column'],
			'contentOptions' => ['class' => 'text-right serial-column'],
		],
		[
			'contentOptions' => ['class' => 'action-column nowrap text-left'],
			'class' => 'yii\grid\ActionColumn',
			'template' => '{proposal-file-delete}',
			'buttons' => [
				'proposal-file-delete' => function ($url) {
					return Html::a('', $url, [
						'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger',
						'data-method' => 'post',
						'data-confirm' => 'Are you sure you want to delete this item?'
					]);
				},
			],
		],
                // 'id',
		/* [
			'attribute' => 'tanggal_upload',
			'format' => 'date',
		], */
		[
			'attribute' => 'file_proposal',
			'format' => 'raw',
			'value' => function ($data) {
				return Html::a($data->name, ['download', 'filename' => $data->file_proposal, 'download_name' => $data->name]);
			},
		],
	];
	?>

        <?= GridView::widget([
								'dataProvider' => $dataProvider,
            // 'pjax' => true,
								'hover' => true,
								'striped' => false,
								'bordered' => false,
								'toolbar' => [
									Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
									Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
									'{toggleData}',
								],
								'panel' => false,
								'pjaxSettings' => ['options' => ['id' => 'grid']],
            // 'filterModel' => $searchModel,
								'columns' => $gridColumns,
							]); ?>

		</div>

	
	


		<br><h3>REVIEW</h3>
		<?= DetailView::widget([
			'options' => ['class' => 'table detail-view'],
			'model' => $model,
			'attributes' => [
				'tanggal_disposisi',
				'reviewerUser.username:text:Reviewer',
				'tanggal_review:date',
				[
					'attribute' => 'hasil_review',
					'value' => $model->hasil_review ? $model->hasilReviews($model->hasil_review) : '',
				],
				'keterangan_review:ntext',
				[
					'attribute' => 'file_review',
					'format' => 'raw',
					'value' => Html::a($model->file_review, ['download', 'filename' => $model->file_review]),
				],
			],
		]) ?>

		</div>

</div>
