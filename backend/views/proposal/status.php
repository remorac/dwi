<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use backend\models\ReviewerUser;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Create Proposal';
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">

		<div class="proposal-form">

			<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

			<?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'dosen_user_id')->widget(Select2::classname(), [
						'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					]); ?>

			<?= $form->field($model, 'jenis_kegiatan_id')->widget(Select2::classname(), [
						'data' => ArrayHelper::map(JenisKegiatan::find()->all(), 'id', 'nama'),
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					]); ?>

			<?= $form->field($model, 'tanggal_pengajuan')->widget(DatePicker::classname(), [
						'type' => DatePicker::TYPE_COMPONENT_PREPEND,
						'readonly' => true,
						'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
					]); ?>

			<?= $form->field($model, 'total_biaya')->textInput() ?>

			<?= $form->field($model, 'sumber_dana_id')->widget(Select2::classname(), [
						'data' => ArrayHelper::map(SumberDana::find()->all(), 'id', 'nama'),
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					]); ?>

			<?= $form->field($model, 'luaran')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'file_proposal')->textarea(['rows' => 6]) ?>

			<?= $form->field($model, 'tanggal_disposisi')->widget(DatePicker::classname(), [
						'type' => DatePicker::TYPE_COMPONENT_PREPEND,
						'readonly' => true,
						'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
					]); ?>

			<?= $form->field($model, 'reviewer_user_id')->widget(Select2::classname(), [
						'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					]); ?>

			<?= $form->field($model, 'tanggal_review')->widget(DatePicker::classname(), [
						'type' => DatePicker::TYPE_COMPONENT_PREPEND,
						'readonly' => true,
						'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
					]); ?>

			<?= $form->field($model, 'hasil_review')->textInput() ?>

			<?= $form->field($model, 'keterangan_review')->textarea(['rows' => 6]) ?>

			<?= $form->field($model, 'file_review')->textarea(['rows' => 6]) ?>

			<?= $form->field($model, 'tanggal_laporan_akhir')->widget(DatePicker::classname(), [
						'type' => DatePicker::TYPE_COMPONENT_PREPEND,
						'readonly' => true,
						'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
					]); ?>

			<?= $form->field($model, 'file_laporan_akhir')->textarea(['rows' => 6]) ?>

			<?= $form->field($model, 'status_laporan_akhir')->textInput() ?>

			<?= $form->field($model, 'file_luaran')->textarea(['rows' => 6]) ?>

			
			<div class="form-panel">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>

		</div>

    </div>

</div>
