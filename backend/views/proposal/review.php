<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Review';
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="proposal-create box box-success">
	<!-- <div class="box-header"></div> -->

    <div class="box-body">

		<?= DetailView::widget([
			'options' => ['class' => 'table detail-view'],
			'model' => $model,
			'attributes' => [
						// 'id',
                'kode',
				'judul',
				'ringkasan:ntext',
				'dosenUser.longText:text:Dosen',
				'jenisKegiatan.nama:text:Jenis Kegiatan',
				'tanggal_pengajuan:date',
				'total_biaya:integer',
				'sumberDana.nama:text:Sumber Dana',
			],
		]) ?>
		<br>

		<?= DetailView::widget([
			'options' => ['class' => 'table detail-view'],
			'model' => $model,
			'attributes' => [
				'tanggal_disposisi:date',
				'reviewerUser.longText:text:Reviewer',
			],
		]) ?>
		<br>
		<br>
		
		<div class="proposal-form">

		    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

			<?= $form->field($model, 'tanggal_review')->widget(DatePicker::classname(), [
						'type' => DatePicker::TYPE_COMPONENT_PREPEND,
						'readonly' => true,
						// 'type' => DateControl::FORMAT_DATETIME,
						// 'autoWidget' => false,
						'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
					]); ?>

			<?= $form->field($model, 'hasil_review')->dropDownList($model->hasilReviews(), ['prompt' => '']) ?>

			<?= $form->field($model, 'file_review')->widget(FileInput::classname(), [
			'options' => [
				'accept' => 'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			],
			'pluginOptions' => [
				'showPreview' => false,
				'showCaption' => true,
				'showRemove' => true,
				'showUpload' => false,
				'allowedFileExtensions' => ['pdf', 'PDF', 'docx', 'DOCX', 'doc', 'DOC'],
				// 'maxFileSize' => 8192,
			]
		]); ?>

		<?= $form->field($model, 'file_penilaian')->widget(FileInput::classname(), [
		'options' => [
			'accept' => 'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		],
		'pluginOptions' => [
			'showPreview' => false,
			'showCaption' => true,
			'showRemove' => true,
			'showUpload' => false,
			'allowedFileExtensions' => ['pdf', 'PDF', 'docx', 'DOCX', 'doc', 'DOC'],
				// 'maxFileSize' => 8192,
		]
		]); ?>

			<?= $form->field($model, 'keterangan_review')->textarea(['rows' => 6]) ?>

			
			<div class="form-panel">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Review'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>

		</div>

    </div>

</div>
