<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProposalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'judul') ?>

    <?= $form->field($model, 'dosen_user_id') ?>

    <?= $form->field($model, 'jenis_kegiatan_id') ?>

    <?= $form->field($model, 'tanggal_pengajuan') ?>

    <?php // echo $form->field($model, 'total_biaya') ?>

    <?php // echo $form->field($model, 'sumber_dana_id') ?>

    <?php // echo $form->field($model, 'luaran') ?>

    <?php // echo $form->field($model, 'file_proposal') ?>

    <?php // echo $form->field($model, 'tanggal_disposisi') ?>

    <?php // echo $form->field($model, 'reviewer_user_id') ?>

    <?php // echo $form->field($model, 'tanggal_review') ?>

    <?php // echo $form->field($model, 'hasil_review') ?>

    <?php // echo $form->field($model, 'keterangan_review') ?>

    <?php // echo $form->field($model, 'file_review') ?>

    <?php // echo $form->field($model, 'tanggal_laporan_akhir') ?>

    <?php // echo $form->field($model, 'file_laporan_akhir') ?>

    <?php // echo $form->field($model, 'status_laporan_akhir') ?>

    <?php // echo $form->field($model, 'file_luaran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
