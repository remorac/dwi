<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\Luaran;
use backend\models\SumberDana;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Luaran Proposal';
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul, 'url' => ['view', 'id' => $model->id]];
?>
<div class="proposal-create box box-success">
	<!-- <div class="box-header"></div> -->

    <div class="box-body">

		<?= DetailView::widget([
		'options' => ['class' => 'table detail-view'],
		'model' => $model,
		'attributes' => [
							// 'id',
            'kode',
			'judul',
			'ringkasan:ntext',
			'dosenUser.username:text:Dosen',
			'jenisKegiatan.nama:text:Jenis Kegiatan',
			'tanggal_pengajuan',
			'total_biaya:integer',
			'sumberDana.nama:text:Sumber Dana',
		],
	]) ?>
		<br>
		<br>

		<div class="proposal-form">

			<?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

			<?= $form->field($modelLK, 'luaran_id')->dropDownList(ArrayHelper::map(Luaran::find()->all(), 'id', 'nama'), ['prompt' => '']); ?>
			
			<div class="form-panel">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($modelLK->isNewRecord ? 'Create' : 'Update'), ['class' => $modelLK->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
		
		<div style="padding-top:20px">

		<?php 
	$gridColumns = [
		[
			'class' => 'yii\grid\SerialColumn',
			'headerOptions' => ['class' => 'text-right serial-column'],
			'contentOptions' => ['class' => 'text-right serial-column'],
		],
		[
			'contentOptions' => ['class' => 'action-column nowrap text-left'],
			'class' => 'yii\grid\ActionColumn',
			'template' => '{proposal-luaran-delete} {link-luaran}',
			'buttons' => [
				'proposal-luaran-delete' => function ($url) {
					return Html::a('', $url, [
						'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger',
					]);
				},
				'link-luaran' => function ($url, $model) {
					return Html::a('', 'javascript:preLink(' . $model->id . ', "' . $model->link_luaran . '", "' . $model->keterangan . '")', [
						'class' => 'glyphicon glyphicon-link btn btn-xs btn-default btn-text-warning',
					]);
				},
			],
		],
				// 'id',
		'luaran.nama:text:Jenis Luaran',
		'link_luaran',
		[
			'attribute' => 'file_luaran',
			'format' => 'raw',
			'value' => function ($data) {
				return $data->file_luaran ? Html::a($data->name, ['download', 'filename' => $data->file_luaran, 'download_name' => $data->name]) : '';
			},
		],
		'keterangan',
	];
	?>

        <?= GridView::widget([
								'dataProvider' => $dataProvider,
            // 'pjax' => true,
								'hover' => true,
								'striped' => false,
								'bordered' => false,
								'toolbar' => [
									Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
									Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
									'{toggleData}',
								],
								'panel' => false,
								'pjaxSettings' => ['options' => ['id' => 'grid']],
            // 'filterModel' => $searchModel,
								'columns' => $gridColumns,
							]); ?>

		</div>
		

	</div>

</div>


<?php 
$js = 
<<<JAVASCRIPT

	function resetAllModals() {
        $("button[type=reset]").click();
    }

	function preLink(id, name, keterangan) {
		resetAllModals();
        $("#link_id").val(id);
        $("#link_luaran").val(name);
        $("#keterangan").val(keterangan);
        $("#modalLink").modal("show");
	}
JAVASCRIPT;

$this->registerJs($js, \yii\web\VIEW::POS_END);
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="modal fade" id="modalLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog" role="document">
            <div class="modal-content panel panel-warning">
                <div class="modal-header panel-heading">
                    <h4 class="modal-title" id="modalLinkTitle">Link Luaran</h4>
                </div>
                <div class="modal-body">
					<?= Html::hiddenInput('link_id', '', ['id' => 'link_id']); ?>
                    <table class="detail-view table">
                        <tr>
                            <th style="width:1px; white-space:nowrap; vertical-align:middle">Link Luaran</th>
                            <td>
                                <?= Html::textInput('link_luaran', '', ['class' => 'form-control', 'id' => 'link_luaran']); ?>
                            </td>
                        </tr>
                        <tr>
                            <th style="width:1px; white-space:nowrap; vertical-align:middle">File Luaran</th>
                            <td>
                                <?= Html::fileInput('file_luaran'); ?>
                            </td>
                        </tr>
                        <tr>
                            <th style="width:1px; white-space:nowrap; vertical-align:middle">Keterangan</th>
                            <td>
                                <?= Html::textInput('keterangan', '', ['class' => 'form-control', 'id' => 'keterangan']); ?>
                            </td>
                        </tr>
                    </table>
                    <div class="text-right" style="border-top:none;">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default', 'style' => 'display:none']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>