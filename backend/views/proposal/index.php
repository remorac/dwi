<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use backend\models\Proposal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProposalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proposals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="proposal-index box box-primary box-body" style="width:auto">

    <?php 
        $exportColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'id',
            'judul',
            'dosenUser.name:text:Dosen',
            'jenisKegiatan.name:text:Jenis kegiatan',
            'tanggal_pengajuan:date',
            'total_biaya',
            'sumberDana.name:text:Sumber dana',
            'luaran',
            'file_proposal',
            'tanggal_disposisi:date',
            'reviewerUser.name:text:Reviewer',
            'tanggal_review:date',
            'hasil_review:integer',
            'keterangan_review',
            'file_review',
            'tanggal_laporan_akhir:date',
            'file_laporan_akhir',
            'status_laporan_akhir:integer',
            'file_luaran',
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'filename' => 'Proposals',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);
        
        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right serial-column'],
                'contentOptions' => ['class' => 'text-right serial-column'],
            ],
            [
                'contentOptions' => ['class' => 'action-column nowrap text-left'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {disposisi} {review} {laporan-akhir}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info', 
                            'title' => 'View',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        $visibility = ['hidden', 'visible'];
                        $index = ($model->hasil_review > 0 || $model->dosen_user_id != Yii::$app->user->id) ? 0 : 1;
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning', 
                            'title' => 'Update',
                            'style' => 'visibility:'.$visibility[$index],
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        $visibility = ['hidden', 'visible'];
                        $index = ($model->reviewer_user_id || $model->dosen_user_id != Yii::$app->user->id) ? 0 : 1;
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
                            'data-method' => 'post', 
                            'data-confirm' => 'Are you sure you want to delete this item?', 'title' => 'Delete',
                            'style' => 'visibility:'.$visibility[$index],
                        ]);
                    },
                    'disposisi' => function ($url, $model) {
                        $visibility = ['hidden', 'visible'];
                        $index = $model->reviewer_user_id ? 0 : 1;
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-tag btn btn-xs btn-default btn-text-success', 
                            'title' => 'Disposisi',
                            'style' => 'visibility:'.$visibility[$index],
                        ]);
                    },
                    'review' => function ($url, $model) {
                        $visibility = ['hidden', 'visible'];
                        $index = !$model->reviewer_user_id ? 0 : 1;
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-th-list btn btn-xs btn-default btn-text-success', 
                            'title' => 'Review',
                            'style' => 'visibility:' . $visibility[$index],
                        ]);
                    },
                    'laporan-akhir' => function ($url, $model) {
                        $visibility = ['hidden', 'visible'];
                        $index = !$model->hasil_review ? 0 : 1;
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-file btn btn-xs btn-default btn-text-success', 
                            'title' => 'Laporan Akhir',
                            'style' => 'visibility:' . $visibility[$index],
                        ]);
                    },
                ],
            ],
            // 'id',
            [
                'header' => 'Proses Berikutnya',
                'contentOptions' => ['style' => 'width:1px'],
                'value' => 'nextProcess',
            ],
            [
                'attribute' => 'kode',
                'contentOptions' => ['style' => 'width:1px'],
            ],
            [
                'attribute' => 'judul',
                'contentOptions' => ['style' => 'min-width:200px'],
            ],
            [
                'attribute' => 'ringkasan',
                'contentOptions' => ['style' => 'min-width:300px'],
            ],
            [
                'attribute' => 'dosen_user_id',
                'format' => 'html',
                'value' => 'dosenUser.longText',
                /* 'value' => function($model) {
                    $anggotas = [];
                    foreach ($model->anggotas as $anggota) {
                        $anggotas[] = $anggota->nama_anggota;
                    }
                    $agt = $anggotas ? '<small><br>Anggota:<br> - '.implode('<br> - ', $anggotas).'</small>' : '';
                    return $model->dosenUser->longText.$agt;
                }, */
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->orderBy('username')->asArray()->all(), 'id', 'full_name'), 
                'filterInputOptions'=>['placeholder'=>''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
            ],
            [
                'attribute' => 'anggotaText',
                'label' => 'Anggota',
                'format' => 'html',
                'headerOptions' => ['style' => 'color:#3c8dbc'],
            ],
            [
                'attribute' => 'jenis_kegiatan_id',
                'value' => 'jenisKegiatan.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(JenisKegiatan::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'), 
                'filterInputOptions'=>['placeholder'=>''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
            ],
            [
                'attribute' => 'tanggal_pengajuan',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'total_biaya',
                'format' => 'integer',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'sumber_dana_id',
                'value' => 'sumberDana.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SumberDana::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'), 
                'filterInputOptions'=>['placeholder'=>''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
            ],
            [
                'attribute' => 'tanggal_disposisi',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'reviewer_user_id',
                'value' => 'reviewerUser.longText',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->orderBy('username')->asArray()->all(), 'id', 'full_name'),
                'filterInputOptions'=>['placeholder'=>''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
            ],
            [
                'attribute' => 'tanggal_review',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'hasil_review',
                'value' => function ($model) {
                    return $model->hasil_review ? $model->hasilReviews($model->hasil_review) : '';
                },
                'filter' => Proposal::hasilReviews(),
            ],
            [
                'attribute' => 'keterangan_review',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'file_review',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-wrap'],
                'value' => function ($model) {
                    return Html::a($model->file_review, ['download', 'filename' => $model->file_review]);
                }
            ],
            [
                'attribute' => 'tanggal_laporan_akhir',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'file_laporan_akhir',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-wrap'],
                'value' => function ($model) {
                    return Html::a($model->file_laporan_akhir, ['download', 'filename' => $model->file_laporan_akhir]);
                }
            ],
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'responsiveWrap' => false,
        'responsive' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            // $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings' => ['options' => ['id' => 'grid']],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>