<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use backend\models\User;
use backend\models\JenisKegiatan;
use backend\models\SumberDana;
use backend\models\Luaran;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'ringkasan')->textarea(['rows' => 2]) ?>

    <?php /*echo $form->field($model, 'dosen_user_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]);*/ ?>

    <?= $form->field($model, 'jenis_kegiatan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(JenisKegiatan::find()->all(), 'id', 'nama'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'tanggal_pengajuan')->widget(DateControl::classname(), [
        // 'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true,/*  'format' => 'yyyy-mm-dd' */],
    ]); ?>

    <?= $form->field($model, 'total_biaya')->textInput() ?>

    <?= $form->field($model, 'sumber_dana_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(SumberDana::find()->all(), 'id', 'nama'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'luarans')->checkBoxList(ArrayHelper::map(Luaran::find()->all(), 'id', 'nama'))->label('Luaran') ?>

    <?= $form->field($model, 'file_proposal')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ],
        'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'allowedFileExtensions' => ['pdf', 'PDF', 'docx', 'DOCX', 'doc', 'DOC'],
				// 'maxFileSize' => 8192,
        ]
    ]); ?>
    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-6 col-sm-offset-3">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
