<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\SumberDana;
use backend\models\JenisKegiatan;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $model backend\models\SumberDana */

$this->title = 'Report: ' . $title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sumber-dana-create box box-success">	
	<div class="box-header with-border">

		<?php if (!$to_pdf) { ?>
			<style type="text/css">
				.select2-container--krajee {
					display: inline-block;
					width: auto !important;
				}
			</style>

			<?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::to(['accepted']), 'options' => ['style' => 'display:inline']]); ?>
		
			<?= Html::textInput('tahun', $tahun, ['placeholder' => 'SEMUA TAHUN', 'class' => 'form-control', 'style' => 'width: 150px; display:none; vertical-align:bottom']); ?>
			
			<?= Select2::widget([
			'name' => 'sumber_dana_id',
			'value' => $sumber_dana_id,
			'data' => array_replace(['' => 'SEMUA SUMBER DANA'], ArrayHelper::map(SumberDana::find()->all(), 'id', 'nama')),
		]); ?>

			<?= Select2::widget([
			'name' => 'jenis_kegiatan_id',
			'value' => $jenis_kegiatan_id,
			'data' => array_replace(['' => 'SEMUA JENIS KEGIATAN'], ArrayHelper::map(JenisKegiatan::find()->all(), 'id', 'nama')),
		]); ?>
			
			<?= Html::button('<i class="glyphicon glyphicon-refresh"></i> ' . Yii::t('app', 'Refresh'), [
			'type' => 'submit',
			'class' => 'btn btn-primary',
		]) ?>
			<?php ActiveForm::end(); ?>
				

			<?= Html::a('<i class="glyphicon glyphicon-print"></i> Print', [
				'accepted',
				'to_pdf' => 1,
				'tahun' => $tahun,
				'sumber_dana_id' => $sumber_dana_id,
				'jenis_kegiatan_id' => $jenis_kegiatan_id,
			], [
				'class' => 'btn btn-default',
				'target' => '_blank',
				'style' => 'display:none',
			]); ?>

			<a href="javascript:window.print()" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Print</a>
			
			<div class="small pull-right text-right col-md-3">Proposal yang hasil reviewnya OK, dan status kegiatan sedang berjalan (belum ada laporan akhir).</div>
			
		<?php 
} ?>
	</div>

    <div class="box-body printable">

		<?= \backend\helpers\ReportHelper::header($title) ?>

		<div class="table-responsive">
	    <table class="table table-report table-bordered">
			<tr>
				<th>No</th>
				<th>Kode</th>
				<th style="min-width:200px">Judul</th>
				<th>Dosen</th>
				<th>Anggota</th>
				<th>Jenis Kegiatan</th>
				<th>Sumber Dana</th>
				<th>Tanggal Review</th>
				<th>Reviewer</th>
				<th class="text-right">Total Biaya</th>
			</tr>
			
			<?php 
		$no = 0;
		$total = 0;
		foreach ($models as $model) {
			$no++;
			$total += $model->total_biaya;
			?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $model->kode ?></td>
					<td><?= $model->judul ?></td>
					<td><?= $model->dosenUser->full_name ?></td>
					<td><?= $model->anggotaText ?></td>
					<td><?= $model->jenisKegiatan->nama ?></td>
					<td><?= $model->sumberDana->nama ?></td>
					<td><?= Yii::$app->formatter->asDate($model->tanggal_review) ?></td>
					<td><?= $model->reviewerUser->full_name ?></td>
					<td class="text-right"><?= Yii::$app->formatter->asDecimal($model->total_biaya, 0) ?></td>
				</tr>
			<?php 
	} ?>
			<tr>
				<td colspan="9"><b>TOTAL</b></td>
				<td class="text-right"><b><?= Yii::$app->formatter->asDecimal($total, 0) ?></b></td>
			</tr>
		</table>
		</div>
		<?= \backend\helpers\ReportHelper::footer() ?>
    </div>

</div>
