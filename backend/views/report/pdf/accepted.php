<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SumberDana */

$this->title = $title;
?>

<table class="table table-report">
	<tr>
		<th>No</th>
		<th>Judul</th>
		<th>Dosen</th>
		<th>Jenis Kegiatan</th>
		<th>Tanggal Pengajuan</th>
		<th>Sumber Dana</th>
		<th>Rencana Luaran</th>
		<th class="text-right">Total Biaya</th>
	</tr>
	
	<?php 
$no = 0;
$total = 0;
foreach ($models as $model) {
	$no++;
	$total += $model->total_biaya;
	?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $model->judul ?></td>
			<td><?= $model->dosenUser->username ?></td>
			<td><?= $model->jenisKegiatan->nama ?></td>
			<td><?= $model->tanggal_pengajuan ?></td>
			<td><?= $model->sumberDana->nama ?></td>
			<td><?= $model->luaran ?></td>
			<td class="text-right"><?= Yii::$app->formatter->asDecimal($model->total_biaya, 0) ?></td>
		</tr>
	<?php 
} ?>
	<tr>
		<td colspan="7"><b>TOTAL</b></td>
		<td class="text-right"><b><?= Yii::$app->formatter->asDecimal($total, 0) ?></b></td>
	</tr>
</table>