<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SumberDana */

$this->title = 'Report: ' . $title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sumber-dana-create box box-success">
	<div class="box-header">
		<p>
			<?= Html::a('<i class="glyphicon glyphicon-print"></i> Print', ['pengajuan', 'to_pdf' => 1], [
			'class' => 'btn btn-default',
			'target' => '_blank',
		]); ?>
		</p>
	</div>

    <div class="box-body">

		<?= \backend\helpers\ReportHelper::header($title) ?>

	    <table class="table table-report">
			<tr>
				<th>No</th>
				<th>Judul</th>
				<th>Dosen</th>
				<th>Jenis Kegiatan</th>
				<th>Tanggal Pengajuan</th>
				<th>Sumber Dana</th>
				<th>Rencana Luaran</th>
				<th class="text-right">Total Biaya</th>
			</tr>
			
			<?php 
		$no = 0;
		$total = 0;
		foreach ($models as $model) {
			$no++;
			$total += $model->total_biaya;
			?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $model->judul ?></td>
					<td><?= $model->dosenUser->username ?></td>
					<td><?= $model->jenisKegiatan->nama ?></td>
					<td><?= $model->tanggal_pengajuan ?></td>
					<td><?= $model->sumberDana->nama ?></td>
					<td><?= $model->luaran ?></td>
					<td class="text-right"><?= Yii::$app->formatter->asDecimal($model->total_biaya, 0) ?></td>
				</tr>
			<?php 
	} ?>
			<tr>
				<th colspan="7">TOTAL</th>
				<th class="text-right"><?= Yii::$app->formatter->asDecimal($total, 0) ?></th>
			</tr>
	    </table>
    </div>

</div>
