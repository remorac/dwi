<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\JenisKegiatan */

$this->title = 'Create Jenis Kegiatan';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-kegiatan-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
