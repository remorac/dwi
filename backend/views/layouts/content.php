<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (Yii::$app->params['showTitle']) { ?>
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
                <?php /*if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'index') { 
                    echo Html::a('<i class="fa fa-refresh"></i>&nbsp;&nbsp;Refresh Data', ['reset-in-progress-schedules'], [
                        'class' => 'btn btn-warning pull-right'
                    ]);
                }*/ ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
        <?php } ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
    </div>
    <strong>Dwi Andalusia &copy; <small><?= (date('Y') > 2018 ? '2018 - ': '').date('Y') ?></small>.</strong> 
    <small>
        STMIK INDONESIA.
    </small>
</footer>
