<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel" style="display:none">
            <div class="pull-left image">
                <img src="img/user.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::$app->user->identity->email ?></a>
            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?php   
            $menuItems = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                ['label' => 'Home', 'icon' => 'fa fa-home', 'url' => ['/site/index']],
                ['label' => 'PROPOSAL', 'icon' => 'fa fa-book', 'url' => ['/proposal/index']],
                ['label' => 'Sumber Dana', 'icon' => 'fa fa-money', 'url' => ['/sumber-dana/index']],
                ['label' => 'Jenis Kegiatan', 'icon' => 'fa fa-cubes', 'url' => ['/jenis-kegiatan/index']],
                ['label' => 'Jenis Luaran', 'icon' => 'fa fa-arrow-right', 'url' => ['/luaran/index']],
                ['label' => 'User', 'icon' => 'fa fa-user', 'url' => ['/user/index']],
                // ['label' => 'Laporan Kemajuan', 'icon' => 'fa fa-th-list', 'url' => ['/laporan-kemajuan/index']],
                [
                    'label' => 'Report',
                    'icon' => 'fa fa-bar-chart',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        ['label' => 'Pengajuan', 'url' => ['/report/pengajuan']],
                        ['label' => 'Review', 'url' => ['/report/review']],
                        ['label' => 'Accepted', 'url' => ['/report/accepted']],
                        ['label' => 'Closed', 'url' => ['/report/closed']],
                        // ['label' => 'Count', 'url' => ['/report/count']],
                        ['label' => 'Luaran', 'url' => ['/report/luaran']],
                    ],
                ],
                ['label' => 'Informasi', 'icon' => 'fa fa-th-list', 'url' => ['/informasi/index']],
                // ['label' => 'Chat', 'icon' => 'fa fa-commenting-o', 'url' => 'http://localhost/phpfreechat/examples/'],
                /* [
                    'label' => 'Pengaturan Data',
                    'icon' => 'fa fa-gear',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        ['label' => 'User', 'url' => ['/user/index']],
                        ['label' => 'Sumber Dana', 'url' => ['/sumber-dana/index']],
                        ['label' => 'Jenis Kegiatan', 'url' => ['/jenis-kegiatan/index']],
                    ],
                ], */
                /* [
                    'label' => 'Access Control',
                    'icon' => 'fa fa-lock',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'visible' => Yii::$app->user->can('superuser'),
                    'items' => [
                        ['label' => 'User',         'url' => ['/user/index']],
                        ['label' => 'Assignment',   'url' => ['/acf/assignment']],
                        ['label' => 'Role',         'url' => ['/acf/role']],
                        ['label' => 'Permission',   'url' => ['/acf/permission']],
                        ['label' => 'Route',        'url' => ['/acf/route']],
                        ['label' => 'Rule',         'url' => ['/acf/rule']],
                    ],
                ], */
                // ['label' => 'Log', 'icon' => 'fa fa-clock-o', 'url' => ['/log/index']],
            ];

            // $menuItems = mdm\admin\components\Helper::filter($menuItems);
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
