<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\LaporanKemajuan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Laporan Kemajuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="laporan-kemajuan-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'proposal.judul:text:Proposal',
                'tanggal_laporan',
                'monev:ntext',
                'file_laporan:ntext',
                'file_monev:ntext',
            ],
        ]) ?>
    </div>
</div>
