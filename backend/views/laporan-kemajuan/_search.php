<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LaporanKemajuanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laporan-kemajuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'proposal_id') ?>

    <?= $form->field($model, 'tanggal_laporan') ?>

    <?= $form->field($model, 'monev') ?>

    <?= $form->field($model, 'file_laporan') ?>

    <?php // echo $form->field($model, 'file_monev') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
