<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LaporanKemajuan */

$this->title = 'Create Laporan Kemajuan';
$this->params['breadcrumbs'][] = ['label' => 'Laporan Kemajuans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-kemajuan-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
