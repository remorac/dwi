<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ProposalFile */

$this->title = 'Update Proposal File: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proposal Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proposal-file-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
