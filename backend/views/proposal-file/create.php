<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ProposalFile */

$this->title = 'Create Proposal File';
$this->params['breadcrumbs'][] = ['label' => 'Proposal Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-file-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
