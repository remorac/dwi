<?php

use yii\helpers\ArrayHelper;
use kartik\tree\Module;
use kartik\tree\TreeView;
use backend\models\Tree;


$this->title = Yii::t('app', 'Indicators');
$this->params['breadcrumbs'][] = $this->title;
 
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Tree model you have in the previous step
    'query' => Tree::find()->addOrderBy('root, lft'), 
    'headingOptions' => ['label' => 'Indicators'],
    'rootOptions' => ['label'=>'<span class="text-primary">Indicators</span>'],
    'topRootAsHeading' => true,
    'fontAwesome' => true,     // optional
    'isAdmin' => true,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => false,       // defaults to true
    'cacheSettings' => [        
        'enableCache' => false   // defaults to true
    ],
    'nodeAddlViews' => [Module::VIEW_PART_2 => '@backend/views/tree/_treePart2'],
    'iconEditSettings' => [
        'show' => 'list',
        'listData' => [
            // 'folder' => 'Folder',
            // 'file' => 'File',
            // 'phone' => 'Phone',
            // 'bell' => 'Bell',
        ]
    ],
]);

?>

<?php 
    $trees = Tree::find()->leaves()->all(); 
    foreach ($trees as $node) {
        $breadcrumbs = [];
        $breadcrumbs['glue'] = ' &raquo; ';
        if (array_key_exists('depth', $breadcrumbs) && $breadcrumbs['depth'] === null) {
            $breadcrumbs['depth'] = '';
        } elseif (!empty($breadcrumbs['depth'])) {
            $breadcrumbs['depth'] = (string) $breadcrumbs['depth'];
        }
        $depth = ArrayHelper::getValue($breadcrumbs, 'depth');
        $glue = ArrayHelper::getValue($breadcrumbs, 'glue');
        $activeCss = ArrayHelper::getValue($breadcrumbs, 'activeCss');
        $untitled = ArrayHelper::getValue($breadcrumbs, 'untitled');
        $name = $node->getBreadcrumbs($depth, $glue, $activeCss, $untitled);
        if ($node->isNewRecord && !empty($parentKey) && $parentKey !== TreeView::ROOT_KEY) {
            /**
             * @var Tree $modelClass
             * @var Tree $parent
             */
            if (empty($depth)) {
                $depth = null;
            }
            if ($depth === null || $depth > 0) {
                $parent = $modelClass::findOne($parentKey);
                $name = $parent->getBreadcrumbs($depth, $glue, null) . $glue . $name;
            }
        }
        // echo $node->name.'<br>';
        echo $name.'<br>';
    }
?>