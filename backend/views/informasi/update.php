<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Informasi */

$this->title = 'Update Informasi: ' . $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Informasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="informasi-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
