<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<style type="text/css">
    body {
        background: #2d42b7 !important;
        color: yellow
    }
    .login-input:focus {
        border-color: #fff !important;
    }
    .btn-flat:hover, .btn-flat:focus {
        background-color:#3f5fd1 !important;
    }
</style>

<p class="login-box-msg" style="margin:50px">
    <img src="<?= Yii::getAlias('@web/img/logo-stmik.png') ?>" height="100px">
    <br>
    <br>
    <span style="font-size:30px"><small>SI PENELITIAN DAN PENGABDIAN MASYARAKAT<br>STMIK INDONESIA PADANG</small></span>
</p>

<div class="login-box" style="margin-top:0">
        
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username'), 'class' => 'login-input', 'style' => '
                width: 100%;
                border-radius:0;
                border:none;
                border-bottom:1px solid #76afd1;
                color: #fff;
                background-color:transparent;
                padding: 10px;
                margin-bottom: 20px;
            ']) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'login-input', 'style' => '
                width: 100%;
                border-radius:0;
                border:none;
                border-bottom:1px solid #76afd1;
                color: #fff;
                background-color:transparent;
                padding: 10px;
                margin-bottom: 30px;
            ']) ?>

        <div class="row">
            <!-- <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div> -->
            <div class="col-xs-12">
                <?= Html::submitButton('MASUK', ['style' => 'background-color:#251f9f; border:none', 'class' => 'btn btn-lg btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

        <p class="text-center">
            <br><br><br><b>Dwi Andalusia &copy; 2018</b>
        </p>
    <!-- </div> -->
</div>