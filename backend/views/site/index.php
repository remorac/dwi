<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\ListView;
use yii\web\JsExpression;
use miloschuman\highcharts\Highcharts;
use kartik\daterange\DateRangePicker;
use backend\models\Proposal;
use backend\models\ProposalLuaran;
use backend\models\Informasi;
use backend\models\InformasiSearch;

/* @var $this yii\web\View */
// Yii::$app->params['showTitle'] = false;

$this->title = 'Home';
Yii::$app->params['showTitle'] = false;

$publikasis = ProposalLuaran::find()->joinWith(['proposal'])->where(['is not', 'tanggal_laporan_akhir', null])->andWhere(['is not', 'link_luaran', null])->all();
$informasis = Informasi::find()->all();

$searchModel = new InformasiSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>

<?php $form = ActiveForm::begin([
    'method' => 'get',
    'action' => Url::base(),
]); ?>

<div class="site-index" style="margin-top:-30px">
    
    <h2 class="text-center" style="width:100%; background:#e9e4ed; border:1px solid #dddd; border-radius:10px; padding:10px; color: purple">INFORMASI</h2>
    <?= (!$informasis) ? 'Belum ada Informasi' : ''; ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'summary' => false,
        'itemView' => function ($model, $key, $index, $widget) {
            return '

            <div class="row">
                <div class="col-md-12">
                    <div class="box" style="border-top:none; border-top-left-radius:8px !important;border-top-right-radius:8px !important;">
                        <div class="box-header" style="background:#f8f8f8; border-top-left-radius:8px;border-top-right-radius:8px;">
                            <div class="box-title">'.$model->judul.'</div>
                        </div>
                        <div class="box-body">
                            '.$model->isi.'
                        </div>
                    </div>
                </div>
            </div>';
        },
    ]) ?>


    <h2 class="text-center" style="width:100%; background:#e9e4ed; border:1px solid #dddd; border-radius:10px; padding:10px; color: purple">PUBLIKASI</h2>
    <?= (!$publikasis) ? 'Belum ada data Publikasi Ilmiah' : ''; ?>
    <div class="row">
    <?php foreach ($publikasis as $publikasi) { ?>
        <div class="col-md-4">
            <div class="box" style="border-top:none; border-top-left-radius:8px !important;border-top-right-radius:8px !important;">
                <div class="box-header" style="background:#f8f8f8; border-top-left-radius:8px;border-top-right-radius:8px;">
                    <div class="box-title"><?= $publikasi->proposal->dosenUser->full_name ?></div>
                </div>
                <div class="box-body">
                    <?= Html::a($publikasi->link_luaran, $publikasi->link_luaran) ?>
                </div>
            </div>
        </div>
    <?php 
    } ?>
    </div>

</div>

<?php ActiveForm::end(); ?>