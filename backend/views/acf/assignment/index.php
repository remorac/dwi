<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('rbac-admin', 'Assignments');
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'headerOptions' => ['style' => 'width:1px'],
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}'
    ],
    [
        'attribute' => $usernameField,
        'value' => function($model) {
            return $model->username.' - '.$model->full_name;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(User::find()->orderBy('username')->asArray()->all(), 'id', 'full_name'),
        'filterInputOptions' => ['placeholder' => ''],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
];
if (!empty($extraColumns)) {
    $columns = array_merge($columns, $extraColumns);
}
?>
<div class="assignment-index box panel-body">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]);
    ?>
    <?php Pjax::end(); ?>

</div>
