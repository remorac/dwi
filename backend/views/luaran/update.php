<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Luaran */

$this->title = 'Update Luaran: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Luaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="luaran-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
