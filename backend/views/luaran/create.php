<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Luaran */

$this->title = 'Create Luaran';
$this->params['breadcrumbs'][] = ['label' => 'Luaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="luaran-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
