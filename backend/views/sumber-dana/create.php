<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SumberDana */

$this->title = 'Create Sumber Dana';
$this->params['breadcrumbs'][] = ['label' => 'Sumber Dana', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sumber-dana-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
