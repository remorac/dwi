<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SumberDana */

$this->title = 'Update Sumber Dana: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sumber Dana', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sumber-dana-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
